from tree_sitter import Language, Parser

Language.build_library(
  # Store the library in the `build` directory
  'vendor/tree-sitter-compiled/my-languages.so',

  # Include one or more languages
  [
    'vendor/tree-sitter-javascript-0.20.1',
  ]
)