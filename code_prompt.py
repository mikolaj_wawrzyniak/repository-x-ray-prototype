import os

from lib import js_analyse, vue_analyse, code_analyse

def get_code_template(code_info):
    # print(code_info)
    code = code_info["code"]
    file_path = code_info["file_path"]

    ext = os.path.splitext(file_path)[1]
    # print(ext)

    function_list = ""
    for function in code_info["tree_info"]["functions"]:
        function_list = function_list + function + "\n"

    # print(function_list)

    class_list = ""
    for class_info in code_info["tree_info"]["classes"]:
        class_list = class_list + class_info + "\n"

    if (ext == ".js"):
        return js_analyse.get_js_prompt(file_path, code, function_list, class_list)
    if (ext == ".vue"):
        return vue_analyse.get_vue_prompt(file_path, code, function_list, class_list)
    else:
        return code_analyse.get_code_file_prompt(file_path, code, function_list, class_list)
    