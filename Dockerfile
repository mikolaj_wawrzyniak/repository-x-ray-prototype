FROM python:3.9-bookworm

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

RUN mkdir vendor && wget -c  https://github.com/tree-sitter/tree-sitter-javascript/archive/refs/tags/v0.20.1.tar.gz -O - | tar -xz -C /usr/src/app/vendor

COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

COPY build_languages.py /usr/src/app/
RUN python3 build_languages.py

COPY scan /usr/src/app/
COPY codeutils.py /usr/src/app/
COPY code_prompt.py /usr/src/app/
COPY lib /usr/src/app/lib

ENV PATH "$PATH:/usr/src/app/"

COPY utils.js /usr/src/app/

CMD ["scan", "$pwd"]
