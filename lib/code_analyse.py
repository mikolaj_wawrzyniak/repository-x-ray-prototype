def get_code_file_prompt(file_path, code):
    print("Code File Prompt")
    analyse_template = f"""
        As a software engineer, your task is to provide a summary of the code file located at '{file_path}' and each individual function within the file. 
       
        <code>
        {code}
        </code>
        
         Please format the summary as valid JSON in the following way:
        {{        
         \"file_summary\": \"(Description of the file content)\",
         \"language\": \"(language)\",
         \"library_imports\": [\"(libraries imported)\"],
        \"file_imports\": [\"(file paths imported)\"],
        \"public_exports\": [\"(classes, functions and variables exported)\"],
        \"functions\": [
          {{
              \"function_name\":\"(Name of the function)\",
              \"function_description\":\"(Description of the function in maximum 5 sentences)\",
              \"function_parameters\": [
                  {{
                      \"name\":\"(Name of the parameter)\",
                      \"description\":\"(description of the parameter)\",
                      \"parameter_type\":"(type of the parameter)\"
                  }}
              ]
          }}
        ]
        }}

        Please ensure that your summary accurately describes the content and purpose of the file and each individual function within it. The summary should be formatted as valid JSON and include details such as the language and type of file. Additionally, please provide information on any library or file imports and function parameters.
        """
    return analyse_template